from sklearn.feature_extraction.text import TfidfVectorizer

from emanual import database


class TextSimilarity(object):

    def __init__(self):
        self.sim_mat = None
        self.vect = None

    def calc_sim_mat(self, X):
        self.vect = TfidfVectorizer(min_df=1)
        tfidf = self.vect.fit_transform(X)
        self.sim_mat = (tfidf * tfidf.T).A
        return self.sim_mat

    def similarity(self, s1, s2):
        if self.sim_mat is not None:
            return self.sim_mat[s1, s2]
        return None

    def similarity_sentences(self, s1, s2):
        pass