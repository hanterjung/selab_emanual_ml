import json
import logging

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from nltk import StanfordTokenizer, StanfordPOSTagger
from nltk.parse.stanford import StanfordDependencyParser

from emanual import settings
from . import constants, nlp, database, clustering, inquiry

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)


# tokenizer, tagger, parser, ...
tokenizer = StanfordTokenizer(path_to_jar=settings.STANFORTAGGER_PATH)
tagger = StanfordPOSTagger(path_to_jar=settings.STANFORTAGGER_PATH, model_filename=settings.STANFORTAGGER_MODEL_PATH)
parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)


def analyze_sent(sent):
    tokens = tokenizer.tokenize(sent)
    tags = tagger.tag(tokens)
    result = parser.tagged_parse(tags)
    dep = next(result)
    roles = [morpheme.split('\t') for morpheme in dep.to_conll(4).split('\n')]

    tagsiter = iter(tags)
    result = []
    for role in roles:
        while True:
            try:
                tag = next(tagsiter)
                if tag[0] == role[0]:
                    result.append([tag[0], tag[1], role[3]])
                    break
                else:
                    result.append([tag[0], tag[1], tag[1]])
            except Exception as e:
                break

    def generalize(result):
        for row in result:
            print(row[1])
        return result

    def assign_role(result):
        gen_result = result.copy()
        for row in gen_result:
            row.append(None)
            if 'subj' in row[2]:
                row[3] = constants.ELEMENT_SUBJECT
            elif 'obj' in row[2]:
                row[3] =  constants.ELEMENT_OBJECT
            elif row[1].startswith('VB') and row[2] == 'aux':
                row[3] = constants.ELEMENT_AUXILIARYVERB
            elif row[1].startswith('VB'):
                row[3] = constants.ELEMENT_VERB
            elif row[1].startswith('RB'):
                row[3] = constants.ELEMENT_ADVERB
            elif 'comp' in row[2] or 'root' in row[2]:
                row[3] = constants.ELEMENT_COMPLEMENT
        return gen_result

    return generalize(assign_role(result))


@csrf_exempt
def handle_convert(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            action = data.get('action')

            if action == 'tokenize':
                sentences_str = data.get('sentences')
                # if not sentences or not section or not pagenum:
                #     return JsonResponse(constants.CODE_FAILURE)
                print(sentences_str)

                # sentences_list = sentences_str.split('\n')
                # print(sentences_list)
                tokenizer = nlp.DocumentTokenizer(sentences_str)
                # sentence_list.extend(tokenizer.sentences)

                sentence_list = tokenizer.sentences
                tokens_list = []
                for sent in sentence_list:
                    if not sent:
                        pass
                    # tags.extend(tokenizer.tags)

                    tokens = analyze_sent(sent)
                    print (tokens)
                    tokens_list.append(tokens)
                print(tokens_list)

                # tokenizer = nlp.DocumentTokenizer(sentences)
                # sentence_list = tokenizer.sentences
                # tags = tokenizer.tags
                # print(tags)

                # ------------------------------------------------------------
                # Do not save the converted data this time.
                # It is saved when a user confirms the result.
                # ------------------------------------------------------------
                # db = database.DBManager()
                # for i in range(0, len(sentence_list)):
                #     rowid = db.save_source_sentence(text=sentence_list[i], user_manual_id=1,
                #                                     section=section, page_num=pagenum)
                #     schema = str(tags[i])
                #     db.save_sentence_schema(schema=schema, source_sentence_id=rowid)
                # db.close()

                return JsonResponse(dict(constants.CODE_SUCCESS,
                                         **{'tokens': tokens_list, 'sentences': sentence_list}))

            elif action == "save":
                tokens = data.get('tokens')
                sentence_list = data.get('sentences')
                section = data.get('section')

                print (tokens)
                print (sentence_list)
                print (section)

                db = database.DBManager()
                for i in range(0, len(sentence_list)):
                    rowid = db.save_source_sentence(text=sentence_list[i], user_manual_id=1,
                                                    section=section)

                    for token in tokens[i]:
                        db.save_morpheme(text=token[0], pos=token[3], source_sentence_id=rowid)

                    schema = str(tokens[i])
                    db.save_sentence_schema(schema=schema, source_sentence_id=rowid)


                db.close()
                return JsonResponse(constants.CODE_SUCCESS)


        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)


@csrf_exempt
def handle_analyze(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            action = data.get('action')

            manual_id = data.get('manual_id')
            if not manual_id:
                manual_id = 1

            if action == 'analyze':
                sc = clustering.SentenceClusterer()
                centroids, clusters = sc.make_cluster(manual_id, k=40)
                rs = sc.extract_sentences(manual_id, centroids, clusters)
                return JsonResponse(dict(constants.CODE_SUCCESS,
                                         **{'result': json.dumps(rs)}))
            elif action == 'analyze_step1':
                # Step 1. Load sentences
                #
                sc = clustering.SentenceClusterer()
                sc.make_cluster_step1(manual_id)
                request.session['sentence_clusterer'] = sc
                return JsonResponse(dict(constants.CODE_SUCCESS))
            elif action == 'analyze_step2':
                # Step 2. Calculate similarity matrix
                #
                sc = request.session['sentence_clusterer']
                sc.make_cluster_step2()
                request.session['sentence_clusterer'] = sc
                return JsonResponse(dict(constants.CODE_SUCCESS))
            elif action == 'analyze_step3':
                # Step 3. Make clusters
                #
                sc = request.session['sentence_clusterer']
                sc.make_cluster_step3(k=3)
                request.session['sentence_clusterer'] = sc
                return JsonResponse(dict(constants.CODE_SUCCESS))
            elif action == 'analyze_step4':
                # Step 4. Save clusters
                #
                sc = request.session['sentence_clusterer']
                sc.make_cluster_step4()
                request.session['sentence_clusterer'] = sc
                return JsonResponse(dict(constants.CODE_SUCCESS))
            elif action == 'pickle':
                sc = clustering.SentenceClusterer()
                centroids, clusters = sc.load_clusters(manual_id)
                rs = sc.extract_sentences(manual_id, centroids, clusters)
                return JsonResponse(dict(constants.CODE_SUCCESS,
                                         **{'result': json.dumps(rs)}))

        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)

def _sentences_in_db_to_dict(sentences):
    sent_list_dict = {}
    for sent in sentences:
        sent_list_dict[sent['id']] = sent['text']
    return sent_list_dict

@csrf_exempt
def handle_inquire(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            action = data.get('action')

            if action == 'inquire':
                sentences = data.get('sentences')
                if not sentences:
                    return JsonResponse(constants.CODE_FAILURE)

                tokenizer = nlp.DocumentTokenizer(sentences)
                # only for one sentence, ignore more sentences
                sentence = tokenizer.sentences[0]
                # tags = tokenizer.tags[0]
                tags = analyze_sent(sentence)
                # request.session['last_sentence'] = sentences
                # request.session['last_tags'] = tags
                return JsonResponse(dict(constants.CODE_SUCCESS,
                                    **{'tokens': [tags], 'sentences': [sentence]}))
            if action == 'answers':
                manual_id = request.GET.get('manual_id')
                sentences = data.get('sentences')
                # tokens = data.get('tokens')
                if not manual_id:
                    manual_id = 1

                # tags = request.session.get('last_tags')
                # if not tags:
                #     return JsonResponse(constants.CODE_FAILURE)
                # print(tags)

                # --------------------------------------------------------
                # Do not retrieve sentence from session.
                # --------------------------------------------------------
                # sentence = request.session.get('last_sentence')
                sentence = sentences[0]
                if not sentence:
                    return JsonResponse(constants.CODE_FAILURE)

                inquirer = inquiry.InquiryHandler()
                answer = inquirer.inquiry(manual_id, sentence);
                return JsonResponse(dict(constants.CODE_SUCCESS,
                                    **{'answers': answer}))
            if action == 'rate':
                pass

        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)



@csrf_exempt
def handle_manage(request):
    if request.method == 'GET':
        action = request.GET.get('action')
        if not action:
            return JsonResponse(constants.CODE_FAILURE)

        if action == 'create':
            manual_name = request.GET.get('manual_name')
            if not manual_name:
                manual_name = '2016 GMC ACADIA OWNERS MANUAL'
            db = database.DBManager()
            db.create_tables()
            db.save_user_manual(name=manual_name)
            rs = db.retrieve_user_manual(name=manual_name)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'manual':
            db = database.DBManager()
            rs = db.retrieve_user_manual()
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'sentence':
            db = database.DBManager()
            manual_id = request.GET.get('manual_id')
            if not manual_id:
                manual_id = 1
            sentence_id = request.GET.get('sentence_id')
            # if not sentence_id:
            #     sentence_id = None
            rs = db.retrieve_source_sentence(user_manual_id=manual_id, source_sentence_id=sentence_id)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'schema':
            db = database.DBManager()
            sentence_id = request.GET.get('sentence_id')
            # if not sentence_id:
            #     sentence_id = None
            schema_id = request.GET.get('schema_id')
            # if not schema_id:
            #     schema_id = None
            rs = db.retrieve_sentence_schema(source_sentence_id=sentence_id, schema_id=schema_id)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))

    return JsonResponse(constants.CODE_FAILURE)