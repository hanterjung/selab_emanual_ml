from django.conf.urls import url
from django.contrib import admin

from . import views, apis

app_name = 'emanual'
urlpatterns = [
    url(r'^test$', views.render_page, {'page': 'test'}, name='test'),

    url(r'^$', views.render_page, {'page': 'main'}, name='main'),
    url(r'^$', views.render_page, {'page': 'main'}, name='index'),


    url(r'^api/convert$', apis.handle_convert),
    url(r'^api/analyze$', apis.handle_analyze),
    url(r'^api/inquire$', apis.handle_inquire),
]
