import numpy as np

from emanual import database
from emanual.clustering import SentenceClusterer
from emanual.sentence_similarity import SentenceSimilarity
from emanual.text_similarity import TextSimilarity


class InquiryHandler(object):

    def __init__(self):
        self.clusterer = SentenceClusterer()
        self.sent_sim = SentenceSimilarity()
        self.text_sim = TextSimilarity()
        self.sim_fn = lambda s1, s2: self.sent_sim.similarity(s1, s2, False)
        # self.sim_fn = self.text_sim.similarity

    def inquiry(self, user_manual_id, question):
        db = database.DBManager()
        sentences = db.retrieve_source_sentence(user_manual_id=user_manual_id)
        centroids, clusters = self.clusterer.load_clusters(user_manual_id)
        # cluster = self._find_cluster(sentences, centroids, question)
        # answers = self._sort_answers(sentences, question, cluster=clusters[cluster])
        answers = self._sort_answers(sentences, question)
        return [sentences[int(s)] for s in answers[:, 0].tolist()]

    def _find_cluster(self, sentences, centroids, question):
        best_sim = -1
        best_cluster = None
        for i, centroid in enumerate(centroids):
            if best_sim <= self.sim_fn(sentences[centroid]['text'], question):
                best_cluster = i
        return best_cluster

    def _sort_answers(self, sentences, question, cluster=None):
        X = []
        sentence_id_list = []
        if cluster is not None:
            X = [sentences[s]['text'] for s in cluster]
            sentence_id_list = cluster
        else:
            X = [s['text'] for s in sentences]
            sentence_id_list = range(len(X))
        X.append(question)
        text_sim = TextSimilarity()
        text_sim.calc_sim_mat(X)

        sim_array = text_sim.sim_mat[len(X)-1, :-1]
        answers = np.column_stack((np.array(sentence_id_list), np.array(sim_array)))
        sorted = answers[answers[:, 1].argsort()[::-1]]

        # sim_array = [self.sim_fn(sentences[s]['text'], question) for s in cluster]
        # answers = np.column_stack((np.array(cluster), np.array(sim_array)))
        # sorted = answers[answers[:, 1].argsort()[::-1]]
        return sorted

if __name__ == '__main__':
    inquery = InquiryHandler()
    s2 = "Is vehicle an electronic?"
    print(inquery.inquiry(1, s2))
