import sqlite3


class DBManager(object):

    def __init__(self):
        self.conn = sqlite3.connect('emanual_db.sqlite3')
        print("Opened database successfully")

    def create_tables(self):
        # self.conn.execute("DROP TABLE IF EXISTS UserManual;")
        # self.conn.execute('''CREATE TABLE UserManual (
        self.conn.execute('''CREATE TABLE IF NOT EXISTS UserManual (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name CHAR(1000)
            )''')

        # self.conn.execute("DROP TABLE IF EXISTS SourceSentence;")
        # self.conn.execute('''CREATE TABLE SourceSentence (
        self.conn.execute('''CREATE TABLE IF NOT EXISTS SourceSentence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text TEXT NOT NULL,
            section CHAR(50),
            page_num INT,
            user_manual_id INT NOT NULL
            )''')

        # self.conn.execute("DROP TABLE IF EXISTS SentenceSchema;")
        # self.conn.execute('''CREATE TABLE SentenceSchema (
        self.conn.execute('''CREATE TABLE IF NOT EXISTS SentenceSchema (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            schema TEXT NOT NULL,
            source_sentence_id INT NOT NULL
            )''')

        # self.conn.execute("DROP TABLE IF EXISTS Morpheme;")
        # self.conn.execute('''CREATE TABLE Morpheme (
        self.conn.execute('''CREATE TABLE IF NOT EXISTS Morpheme (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text CHAR(256),
            pos CHAR(10),
            source_sentence_id INT NOT NULL
            )''')

        self.conn.commit()

        print("Table created successfully")

    def save_user_manual(self, name=None):
        sql = "INSERT INTO UserManual (name) VALUES ('%s')" % name
        return self._save(sql)

    def retrieve_user_manual(self, user_manual_id=None, name=None):
        cols = ('id', 'name')
        sql = None
        if user_manual_id is not None and name is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s AND name='%s'" % (user_manual_id, name)
        elif user_manual_id is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s" % (user_manual_id)
        elif name is not None:
            sql = "SELECT * FROM UserManual WHERE name='%s'" % (name)
        else:
            sql = "SELECT * FROM UserManual"
        return self._retrieve(sql, cols)

    def save_source_sentence(self, text=None, section=None, page_num=None, user_manual_id=None):
        if section is None:
            section = ""
        if page_num is None:
            page_num = 0
        sql = "INSERT INTO SourceSentence (text, section, page_num, user_manual_id) VALUES ('%s', '%s', %s, %s)" % (text, section, page_num, user_manual_id)
        return self._save(sql)

    def retrieve_source_sentence(self, source_sentence_id=None, user_manual_id=None):
        cols = ('id', 'text', 'section', 'page_num', 'user_manual_id')
        sql = None
        if source_sentence_id is not None and user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s AND user_manual_id='%s'" % (source_sentence_id, user_manual_id)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s" % (source_sentence_id)
        elif user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE user_manual_id='%s'" % (user_manual_id)
        else:
            sql = "SELECT * FROM SourceSentence"
        return self._retrieve(sql, cols)

    def save_sentence_schema(self, schema=None, source_sentence_id=None):
        sql = 'INSERT INTO SentenceSchema (schema, source_sentence_id) VALUES ("%s", %s)' % (schema, source_sentence_id)
        return self._save(sql)

    def retrieve_sentence_schema(self, schema_id=None, source_sentence_id=None):
        cols = ('id', 'text', 'section', 'page_num', 'user_manual_id')
        sql = None
        if schema_id is not None and source_sentence_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE id=%s AND source_sentence_id="%s"' % (schema_id, source_sentence_id)
        elif schema_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE id=%s' % (schema_id)
        elif source_sentence_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE source_sentence_id="%s"' % (source_sentence_id)
        else:
            sql = 'SELECT * FROM SentenceSchema'
        return self._retrieve(sql, cols)

    def save_morpheme(self, text=None, pos=None, source_sentence_id=None):
        sql = "INSERT INTO Morpheme (text, pos, source_sentence_id) VALUES ('%s', '%s', %s)" % (text, pos, source_sentence_id)
        return self._save(sql)

    def retrieve_morpheme(self, morpheme_id=None, source_sentence_id=None):
        cols = ('id', 'text', 'section', 'page_num', 'user_manual_id')
        sql = None
        if morpheme_id is not None and source_sentence_id is not None:
            sql = "SELECT * FROM Morpheme WHERE id=%s AND source_sentence_id='%s'" % (morpheme_id, source_sentence_id)
        elif morpheme_id is not None:
            sql = "SELECT * FROM Morpheme WHERE id=%s" % (morpheme_id)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM Morpheme WHERE source_sentence_id='%s'" % (source_sentence_id)
        else:
            sql = "SELECT * FROM Morpheme"
        return self._retrieve(sql, cols)

    def _save(self, sql):
        res = self.conn.execute(sql)
        self.conn.commit()
        return res.lastrowid

    def _retrieve(self, sql, cols):
        rows = self.conn.execute(sql)
        res = []
        for row in rows:
            res.append({k: v for k, v in zip(cols, row)})
        if len(res) == 0:
            return None
        elif len(res) == 1:
            return res[0]
        else:
            return res

    def close(self):
        if self.conn is not None:
            self.conn.close()


if __name__ == '__main__':
    db = DBManager()
    db.create_tables()

    user_manual_id = db.save_user_manual(name='Vehicle Manual')
    source_sentence_id = db.save_source_sentence(text="asdf", section='1', page_num=1, user_manual_id=user_manual_id)
    db.save_sentence_schema(schema='schema test', source_sentence_id=source_sentence_id)
    db.save_morpheme(text='the', pos='aa', source_sentence_id=source_sentence_id)

