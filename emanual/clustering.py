import pickle
import os

import numpy as np
import pandas as pd

from emanual import database
from emanual.sentence_similarity import SentenceSimilarity

from sklearn.feature_extraction.text import TfidfVectorizer

from emanual.text_similarity import TextSimilarity


class SentenceClusterer(object):

    def __init__(self):
        self.sent_sim = SentenceSimilarity()
        self.sim_fn = lambda s1, s2: self.sent_sim.similarity(s1, s2, False)
        self.cache = {}
        self.text_sim = TextSimilarity()
        self.sentences = None
        self.centroids = None
        self.clusters = None
        self.X = None

    def load_sentences(self, user_manual_id):
        db = database.DBManager()
        self.sentences = db.retrieve_source_sentence(user_manual_id=user_manual_id)
        db.close()

    def extract_sentences(self, user_manual_id, centroids, clusters):
        if self.sentences is None:
            self.load_sentences(user_manual_id)
        return (
            [self.sentences[c] for c in centroids],
            [[self.sentences[c] for c in cluster] for cluster in clusters]
        )

    def make_cluster(self, user_manual_id, k=3):
        if self.sentences is None:
            self.load_sentences(user_manual_id)
        X = [s['text'] for s in self.sentences]
        self.text_sim.calc_sim_mat(X)
        centroids, clusters = self.kmeans(X, k)
        pickle.dump((centroids, clusters), open('clusters_manual_%s.pkl' % user_manual_id, 'wb'))
        return centroids.tolist(), clusters.tolist()

    def make_cluster_step1(self, user_manual_id):
        self.load_sentences(user_manual_id)

    def make_cluster_step2(self):
        self.X = [s['text'] for s in self.sentences]
        self.text_sim.calc_sim_mat(self.X)

    def make_cluster_step3(self, k):
        self.centroids, self.clusters = self.kmeans(self.X, k)

    def make_cluster_step4(self):
        pickle.dump((self.centroids, self.clusters), open('clusters_manual_%s.pkl' % user_manual_id, 'wb'))

    def cluster(self, X, k, centroids):
        """ Make 'k' clusters from the dataset, X.
        """
        clusters = {cluster_label: [] for cluster_label in range(k)}  # cluster labels become 0 ... k-1.
        for x in range(len(X)):
            c = np.argmax([self.hit(X, x, y) for y in centroids])
            clusters[c].append(x)
        return clusters

    def kmeans(self, X, k):
        """ Run K-Means clustering with the dataset, X, and the number clusters, k.
        """
        # Init centroids
        centroids = np.random.choice(range(len(X)), k)
        print('Initial centroids are %s' % centroids)

        itr = 0
        while True:
            itr += 1
            print("Iteration %s..." % itr)
            clusters = self.cluster(X, k, centroids)
            updated_centroids = self.update_centroids(X, clusters, centroids)
            diff_centroids = [node for cl, node in enumerate(centroids) if centroids[cl] != updated_centroids[cl]]
            if len(diff_centroids) > 0:
                centroids = updated_centroids
            else:
                return centroids, pd.Series(clusters)

    def update_centroids(self, X, clusters, centroids):
        """ Update the centroids of the clusters.
        """
        updated_centroids = centroids.copy()
        for c, nodes in clusters.items():
            max_sim = 0
            for node in nodes:
                other_nodes = nodes.copy()
                other_nodes.remove(node)
                tmp_sim = 0
                for other_node in other_nodes:
                    tmp_sim += self.hit(X, node, other_node)
                if tmp_sim >= max_sim:
                    max_sim = tmp_sim
                    updated_centroids[c] = node
                    print("For the cluster %s, centroid is changed to the node %s." % (c, node))
        return updated_centroids

    def hit(self, X, s1, s2):
        # if (s1, s2) in self.cache:
        #     return self.cache[(s1, s2)]
        # elif (s2, s1) in self.cache:
        #     return self.cache[(s2, s1)]
        # sim = self.sim_fn(X[s1], X[s2])
        # self.cache[(s1, s2)] = sim
        sim = self.text_sim.similarity(s1, s2)
        return sim

    def load_clusters(self, user_manual_id):
        fname = 'clusters_manual_%s.pkl' % user_manual_id
        if os.path.isfile(fname):
            centroids, clusters = pickle.load(open(fname, 'rb'))
            return centroids.tolist(), clusters.tolist()
        return None


if __name__ == '__main__':
    clusterer = SentenceClusterer()
    user_manual_id = 1
    centroids, clusters = clusterer.make_cluster(1)
    print(clusterer.extract_sentences(centroids, clusters))

    centroids, clusters = clusterer.load_clusters(1)
    print(clusterer.extract_sentences(centroids, clusters))

    # centroids, clusters = clusterer.load_clusters(user_manual_id)
    # db = database.DBManager()
    # sentences = db.retrieve_source_sentence(user_manual_id=user_manual_id)


    # for i, cluster in enumerate(clusters):
    #     print("[Cluster %s]" % i)
    #     for s in cluster:
    #         print(sentences[s]['text'])
    #     print('')
