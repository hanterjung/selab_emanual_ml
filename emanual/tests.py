import logging
import unittest
from unittest import TestCase
import requests

# Create your tests here.

BASE_URL = "http://127.0.0.1:8000/api"



logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)


# @unittest.skip("")
class ConvertTestCase(TestCase):

    def test(self):
        res = requests.post("%s/convert" % BASE_URL, json={
            "action": "tokenize",
            "sentences": "How do I control the front-side mirror?"
        })
        logger.info(res.json())
