import nltk
from nltk.corpus import wordnet as wn

SAMPLE_TEXT = "The vehicle has an electronic shift position indicator within the instrument cluster. " \
              "When using the ERS mode a number will display next to the L, indicating the current gear that has been selected."

class DocumentTokenizer(object):

    def __init__(self, document):
        self.document = document
        self.sentences = nltk.sent_tokenize(self.document)
        self.tokens = [nltk.word_tokenize(sentence) for sentence in self.sentences]
        self.tags = [nltk.pos_tag(tokens_sent) for tokens_sent in self.tokens]
        # self.synonyms = [[[synset for synset in wn.synsets(token)] for token in tokens_sent]
        #                  for tokens_sent in self.tokens]

if __name__ == '__main__':
    document_tokenizer = DocumentTokenizer(SAMPLE_TEXT)
    print(document_tokenizer.sentences)
    print(document_tokenizer.tokens)
    print(document_tokenizer.tags)
    # print(document_tokenizer.synonyms)
    # print(document_tokenizer.synonyms[0][1][0].wup_similarity(document_tokenizer.synonyms[0][2][0]))


