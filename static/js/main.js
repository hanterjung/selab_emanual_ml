/**
 * Created by khan on 2016-04-22.
 */
$(document).ready(function () {
    $('#inquireForm').hide();
    $('#clusterForm').hide();

    setForms();
});

var tokenize_result = {};
var is_convert_menu = true;

function setForms() {
    $('#btnConvert').click(function() {
        tokenize_result = {};
        var sentences = $('#inputOriginSentences').val();
        var section = $('#inputSection').val();

        if (!/\S/.test(sentences)) {
            openAlertModal("There is no sentence.");
            return;
        }

        $.LoadingOverlay('show');
        $.ajax("/api/convert", {
            method: 'POST',
            data: JSON.stringify({
                action: 'tokenize',
                sentences: sentences
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    tokenize_result['tokens'] = res['tokens'];
                    tokenize_result['sentences'] = res['sentences'];
                    tokenize_result['section'] = section;

                    console.log(res['tokens']);
                    console.log(res['sentences']);

                    var convertForm = $('#convertForm');
                    //resetSenForms(convertForm);
                    setSenElTables(convertForm, res['tokens'], res['sentences']);
                    $('#inputOriginSentences, #inputSection').attr('disabled', 'disabled');
                    //convertForm.find('.convertBtnDiv').hide();
                    convertForm.find('.convertBtnDiv').find('button').attr('disabled', 'disabled');
                    convertForm.find('.acceptBtnsDiv').show();
                    $('#convertQueryButtonContainer').show();
                    console.log($('#convertQueryButtonContainer'));
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });

    $('#btnQueryAccept').click(function() {
        $('#convertForm').find('.convertedSentencesContainer')
                .find('.tokenContainer').find('.tokenDiv:not(.ex-token)').each(function() {
            var sentNum = $(this).data('sentNum');
            var tokenNum = $(this).data('tokenNum');
            var token = tokenize_result['tokens'][sentNum][tokenNum];
            token[3] = $(this).find('.selectElem').val();
            if (token[3]=="None") token[3] = null;
            token[1] = $(this).find('.selectTag').val();
        });

        $.LoadingOverlay('hide');

        $.ajax("/api/convert", {
            method: 'POST',
            data: JSON.stringify({
                action: 'save',
                tokens: tokenize_result['tokens'],
                sentences: tokenize_result['sentences'],
                section: tokenize_result['section']
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    $('#btnQueryAccept').attr('disabled', 'disabled');
                    $('#btnQueryRefine').attr('disabled', 'disabled');
                    $('#btnQueryClear').text('Clear');
                    setRefineConvertResult(false);
                    openAlertModal("The convert result is saved successfully.", "Save Success")
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });

    $('#btnConvertInquire').click(function() {
        resetAnswerForm();
        tokenize_result = {};
        var sentences = $('#inquirySentence').val();

        if (!/\S/.test(sentences)) {
            openAlertModal("There is no sentence.");
            return;
        }

        $.LoadingOverlay('show');
        $.ajax("/api/inquire", {
            method: 'POST',
            data: JSON.stringify({
                action: 'inquire',
                sentences: sentences
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    tokenize_result['tokens'] = res['tokens'];
                    tokenize_result['sentences'] = res['sentences'];

                    console.log(res['tokens']);

                    var inquireForm = $('#inquireForm');
                    //resetSenForms(inquireForm);
                    setSenElTables(inquireForm, res['tokens'], res['sentences']);
                    $('#inquirySentence').attr('disabled', 'disabled');
                    //inquireForm.find('.convertBtnDiv').hide();
                    inquireForm.find('.convertBtnDiv').find('button').attr('disabled', 'disabled');
                    inquireForm.find('.acceptBtnsDiv').show();
                    $('#answerButtonContainer').show();
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });

    $('#btnAnswer').click(function() {
        $.LoadingOverlay('show');
        $.ajax("/api/inquire", {
            method: 'POST',
            data: JSON.stringify({
                action: 'answers',
                tokens: tokenize_result['tokens'],
                sentences: tokenize_result['sentences'],
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    console.log(res['tokens']);
                    console.log(res['answers']);
                    //$('#answer').val(res['answer']);
                    setAnswers(res['answers']);
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });

    $('#btnQueryClear, #btnConvertCancel').click(function() {
        tokenize_result = {};
        showConvert();
    });
    $('#btnQueryRefine').click(function() {
        setRefineConvertResult();
    });

    $('#btnInquireClear, #btnInquireClear2, #btnConvertInquireCancel').click(function() {
        tokenize_result = {};
        showInquire();
    });
    $('#btnInquireRefine').click(function() {
        setRefineConvertResult();
    });

    //$('#inputSection').change(function() {
    //    var value = $(this).val();
    //    if(value !='' && value !=null) {
    //        if (value < 1) $(this).val(null);
    //        else if (value > 99999) $(this).val(null);
    //    }
    //});

    showConvert();
    $('.ex-convertedSentences').hide();
    $('.ex-token').hide();
    $('.ex-tr').hide();
    $('.ex-formalExpression').hide();
    $('.ex-answerContainer').hide();
    $('.ex-clusters').hide();
    $('.ex-btn-cluster').hide();
};

function startAnalyze() {
    $.LoadingOverlay('show');
    $.ajax("/api/analyze", {
        method: 'POST',
        data: JSON.stringify({
            action: 'analyze'
        }),
        dataType: 'json',
        success: function (res) {
            $.LoadingOverlay('hide');
            if (res['code'] == 'SUCCESS') {
                //console.log(res['clusters']);
                //console.log(res['sentences']);
                //setClusters(JSON.parse(res['clusters']), res['sentences']);
                setAnalyzeResult(JSON.parse(res['result']));
            } else {
                openAlertModal('Analysis is failed');
            }
        }
    });
}

function loadLearnedModel() {
    $.LoadingOverlay('show');
    $.ajax("/api/analyze", {
        method: 'POST',
        data: JSON.stringify({
            action: 'pickle'
        }),
        dataType: 'json',
        success: function (res) {
            $.LoadingOverlay('hide');
            if (res['code'] == 'SUCCESS') {
                setClusters(JSON.parse(res['result']));
            } else {
                openAlertModal("There is no analyzed clusters. Please analyze first.", "No Clusters");
            }
        }
    });
}

function resetSenForms(form) {
    form.find('.convertedSentencesContainer .convertedSentences').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });
    form.find('.formalExpression').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });
    form.find('.convertedSentencesRow').hide();
    form.find('.acceptBtnsDiv').hide();
    //form.find('.convertBtnDiv').show();
    form.find('.convertBtnDiv').find('button').removeAttr('disabled');
}

function setSenElTables(form, tokensList, sentences) {
    form.find('.convertedSentencesRow').show();
    for (var i=0; i<tokensList.length; i++) {
        var tokens = tokensList[i];
        addSenElTable(form, tokens, sentences[i], i);
    }
}

function addSenElTable(form, tokens, sentence, num) {
    var newSenTable = form.find('.convertedSentencesContainer .ex-convertedSentences').clone()
        .removeClass('ex-convertedSentences').show();
    //console.log(newSenTable);
    newSenTable.appendTo(form.find('.convertedSentencesContainer .convertedSentencesDiv'));

    if(is_convert_menu) {
        newSenTable.find('.sentenceNum').text('Sentence #' + (num + 1) + ': ');
    } else {
        newSenTable.find('.sentenceNum').text('Inquiry Sentence: ');
    }
    newSenTable.find('.sentenceText').text(sentence);
    // var table = newSenTable.find('table');
    var tokenContainer = newSenTable.find('.tokenContainer');

    for (var i=0; i<tokens.length; i++) {
        // addSenElRow(table, tokens[i]);
        addTokenEl(tokenContainer, tokens, num, i);
    }
}

//var previousDet = null;
var passCnt = 0;
function addTokenEl(container, tokens, sentenceNum, tokenNum) {
    if (passCnt > 0) {
        passCnt--;
        return;
    }

    var token = tokens[tokenNum];
    var word = token[0];
    var langTag = token[1];
    var langDetailedElem = token[2];
    var langElem = token[3];

    //관사일경우 다음 명사와 합침.
    if ('det' == langDetailedElem) {
        var nowNum = tokenNum;
        var newWord = word;
        var bFindNoun = false;
        for (var i=nowNum+1; i<tokens.length; i++) {
            newWord += ' ' + tokens[i][0];
            passCnt++;
            //if (tokens[i][1].startsWith('NN')) {
            if (tokens[i][1].substring(0,2) == 'NN') {
                bFindNoun = true;
                langTag = tokens[i][1];
                langDetailedElem = tokens[i][2];
                langElem = tokens[i][3];
                break;
            }
        }
        if (bFindNoun==false) passCnt=0;
        else {
            word = newWord;
            tokenNum = tokenNum+passCnt;
        }
    }

    //if (previousDet != null) {
    //    word = previousDet + ' ' + word;
    //}
    //
    //if ('det' == langDetailedElem) {
    //    previousDet = word;
    //    return;
    //} else {
    //    previousDet = null;
    //}

    var tokenDiv = container.find('.ex-token').clone().removeClass('ex-token')
        .show().css('display', 'inline-block');
    $(tokenDiv).data('sentNum', sentenceNum);
    $(tokenDiv).data('tokenNum', tokenNum);
    tokenDiv.appendTo(container);

    //var wordTokenWidth = 60;
    //if (word.length > 4) wordTokenWidth = word.length * 15;
    // tokenDiv.find('.inputToken').css('width', wordTokenWidth+'px').val(word);

    tokenDiv.find('.inputToken').text(word);
    tokenDiv.find('.selectTag').val(langTag);
    if (langElem == null) langElem = 'None';
    tokenDiv.find('.selectElem').val(langElem);

}

function addSenElRow(table, token) {
    var tbody = table.find('> tbody');
    var row = tbody.find('> .ex-tr').clone().removeClass('ex-tr').show();
    row.appendTo(tbody);
    row.find('.inputToken').val(token[0]);
    row.find('.inputTag').val(token[1]);
}

function resetAnswerForm() {
    $('.answerButtonContainer').hide();
    $('#btnInquireRefine').removeAttr('disabled');
    $('#btnAnswer').removeAttr('disabled');
    $('#btnInquireClear').removeAttr('disabled');
    $('.answerSentenceContainer').hide();
    $('.answerResultButtonContainer').hide();
    //$('#answer').val('');
    $('.answerContainer').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });
}

function setAnswers(answers) {
    $('#btnInquireRefine').attr('disabled', 'disabled');
    $('#btnAnswer').attr('disabled', 'disabled');
    $('#btnInquireClear').attr('disabled', 'disabled');
    $('.answerSentenceContainer').show();
    $('.answerResultButtonContainer').show();
    setRefineConvertResult(false);

    var answerForm = $('.ex-answerContainer').clone().removeClass('ex-answerContainer').show();
        answerForm.appendTo($('.answerSentenceContainer'));
        //answerForm.find('.answerNum').text('Answers');

    var answerText = '';
    for (var i=0; i<answers.length; i++) {
        if (i>=5) break;

        var answer = answers[i];
        //console.log(answer);
        //var answerText = '[' + answer['section'] + '], p.' + answer['page_num'] + '\n' + answer['text'];
        answerText += answer['text'];

        if(answer['section'] == undefined || answer['section'] == null
                || answer['section'] == '0' || answer['section'] == '') {
            answerText += '<br/>'
        } else {
            //answerText += '[ Section #' + answer['section'] + ' ]\n';
            answerText += ' (Section: ' + answer['section'] + ')<br/>';
        }
        //answerForm.find('textarea').val(answerText);
    }
    answerForm.find('.answerArea').html(answerText);
}

function setAnalyzeResult(result) {
    var analyzeForm = $('#analyzeForm');
    analyzeForm.find('.clusters').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });

    var centroids = result[0];
    var clusters = result[1];

    for (var i=0; i<clusters.length; i++) {
        addClusterTable(analyzeForm, i, centroids[i], clusters[i]);
    }
}

var clusterCentorids = [];
var clusters = [];
function setClusters(result) {
    var clusterForm = $('#clusterForm');
    clusterForm.find('.clusters:not(.ex-clusters)').each(function (index, elem) {
        $(elem).remove();
    });
    clusterForm.find('.btn-cluster:not(.ex-btn-cluster)').each(function (index, elem) {
        $(elem).remove();
    });

    clusterCentorids = result[0];
    clusters = result[1];

    var numClusters = clusters.length;
    var numSentences = 0;
    for (var i=0; i<numClusters; i++) {
        numSentences += clusters[i].length;
    }
    var avgSentences = numSentences / numClusters;
    avgSentences = parseInt(avgSentences * 10) / 10;

    var statisticsHtml = 'Number of Sentences: ' + numSentences + '<br/>' +
            'Number of Clusters: ' + numClusters + '<br/>' +
            'Ave. # of Sentences per Cluster: ' + avgSentences;
    clusterForm.find('.clusterStatistics').html(statisticsHtml);


    for (var i=0; i<numClusters; i++) {
        numSentences += clusters[i].length;
        var newClusterButton = clusterForm.find('.ex-btn-cluster').clone().removeClass('ex-btn-cluster').show();
        newClusterButton.text('Cluster ' + i);
        newClusterButton.data('cluster', i);
        newClusterButton.appendTo(clusterForm.find('.buttonsClusterDiv'));
        $(newClusterButton).click(function() {
            clusterForm.find('.btn-cluster').removeClass('selected');
            $(this).addClass('selected');
            var clusterId = $(this).data('cluster');
            $.LoadingOverlay('show');
            setClusterTable(clusterForm, clusterId, clusterCentorids[clusterId], clusters[clusterId]);
            $.LoadingOverlay('hide');
            //$.LoadingOverlay('show');
            //setTimeout(function() {
            //    setClusterTable(clusterId, clusterCentorids[clusterId], clusters[clusterId]);
            //    $.LoadingOverlay('hide');
            //}, 10);
            $(this).blur();
        });
    }
}

function setClusterTable(form, num, centroid, cluster) {
    form.find('.clusters:not(.ex-clusters)').each(function (index, elem) {
        $(elem).remove();
    });

    var newClusterTable = form.find('.ex-clusters').clone().removeClass('ex-clusters').show();
    newClusterTable.appendTo(form.find('.clustersContainer'));

    newClusterTable.find('.clusterNum').text('Sentences in Cluster '+num);
    var table = newClusterTable.find('table');

    for (var i=0; i<cluster.length; i++) {
        var sentId = cluster[i]['id'];
        var sentence = cluster[i]['text'];
        var centroidId = centroid['id'];
        addClusterTableNode(table, sentId, sentence, sentId==centroidId);
    }
}

function addClusterTable(form, num, centroid, cluster) {
    var newClusterTable = form.find('.ex-clusters').clone().removeClass('ex-clusters').show();
    newClusterTable.appendTo(form.find('.clustersContainer'));

    newClusterTable.find('.clusterNum').text('Cluster #'+num);
    var table = newClusterTable.find('table');

    for (var i=0; i<cluster.length; i++) {
        var sentId = cluster[i]['id'];
        var sentence = cluster[i]['text'];
        var centroidId = centroid['id'];
        addClusterTableNode(table, sentId, sentence, sentId==centroidId);
    }
}

function addClusterTableNode(table, id, sentence, centroid) {
    if (centroid == undefined || centroid == null)
        centroid = false;

    var tbody = table.find('> tbody');
    var row = tbody.find('> .ex-tr').clone().removeClass('ex-tr').show();
    row.appendTo(tbody);
    row.find('.inputSentId').val(id);
    row.find('.inputSentence').val(sentence);
    if(centroid) {
        row.addClass('centroid');
        row.find('.inputSentence').val(sentence + ' (Centroid)');
    }
}

function showConvert() {
    is_convert_menu = true;
    tokenize_result = {};
    $('#convertForm').show();
    $('#inquireForm').hide();
    $('#analyzeForm').hide();
    $('#clusterForm').hide();
    resetSenForms($('#convertForm'));
    $('#convertQueryButtonContainer').hide();
    $('#convertForm').find('input, textarea').val('');
    $('#convertForm').find('input, textarea').removeAttr('disabled');
    $('#btnQueryAccept').removeAttr('disabled');
    $('#btnQueryRefine').removeAttr('disabled');
    $('#btnQueryClear').text('Cancel');
    $('.row-menu').find('.btn-menu').removeClass('selected');
    $('#btnConvertMenu').addClass('selected');
}

function showInquire() {
    is_convert_menu = false;
    tokenize_result = {};
    $('#convertForm').hide();
    $('#inquireForm').show();
    $('#analyzeForm').hide();
    $('#clusterForm').hide();
    resetSenForms($('#inquireForm'));
    $('#answerButtonContainer').hide();
    $('#inquireForm').find('input, textarea').val('');
    $('#inquireForm').find('input, textarea').removeAttr('disabled');
    resetAnswerForm();
    $('.row-menu').find('.btn-menu').removeClass('selected');
    $('#btnInquireMenu').addClass('selected');
}

function showAnalyze() {
    $('#convertForm').hide();
    $('#inquireForm').hide();
    $('#analyzeForm').show();
    $('#clusterForm').hide();
    $('.row-menu').find('.btn-menu').removeClass('selected');
    $('#btnAnalyzeMenu').addClass('selected');
    startAnalyze();
}

function showCluters() {
    $('#convertForm').hide();
    $('#inquireForm').hide();
    $('#analyzeForm').hide();
    $('#clusterForm').show();
    $('.row-menu').find('.btn-menu').removeClass('selected');
    $('#btnClustersMenu').addClass('selected');
    loadLearnedModel();
}

function setRefineConvertResult(enable) {
    if (enable == undefined || enable == null)
        enable = true;
    if (enable) {
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectTag').removeAttr('disabled');
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectElem').removeAttr('disabled');
    } else {
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectTag').attr('disabled', 'disabled');
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectElem').attr('disabled', 'disabled');
    }
}

function openAlertModal(msg, title) {
    if (title==undefined || title==null) {
        title = "Alert";
    }
    $('#alertModalTitle').text(title);
    $('#alertModalMsg').text(msg);
    $('#alertModal').modal('show');
}